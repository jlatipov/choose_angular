import { Component, NgZone, Inject, EventEmitter,  Input, Output, OnInit} from '@angular/core';
import { NgUploaderOptions, UploadedFile, UploadRejected } from 'ngx-uploader';

@Component({
  selector: 'app-image-uploader',
  templateUrl: './image-uploader.component.html',
  styleUrls: ['./image-uploader.component.css']
})
export class ImageUploaderComponent implements OnInit {
  @Input() label: string;
  @Input() inputName: string;
  @Input() startUploading: boolean = false;
  @Input() disabled: boolean = false;
  _id: number;

  @Output() onResponse: EventEmitter<any> = new EventEmitter<any>();
  @Output() inputUploadEvents: EventEmitter<string> = new EventEmitter<string>();

  @Input()
  set id(id: number) {
    this._id = id;
  }
  get id(): number { return this._id; }


  options: NgUploaderOptions;
  response: any;
  sizeLimit: number = 4000000; // 1MB
  previewData: any;
  errorMessage: string;
  progress: any = {
    percent: 0,
    total: 0,
    loaded: 0
  };

  constructor(@Inject(NgZone) private zone: NgZone) { }

  fileIsSelected() {
    return this.previewData ? true : false;
  }

  ngOnInit() {
    let opt = {
      url: `${window.location.protocol}//${window.location.host}/api/UploadImage`,
      filterExtensions: true,
      allowedExtensions: ['jpg', 'png', 'jpeg', 'JPG', 'JPEG', 'PNG', 'GIF', 'gif'],
      data: { id: 0 },
      autoUpload: false,
      fieldName: this.inputName,
      fieldReset: true,
      maxUploads: 100,
      method: 'POST',
      previewUrl: true,
      withCredentials: false,
      removeAfterUpload: true,
      sizeLimit: this.sizeLimit,
      fileIsTooLargeMessage: 'Файл больше допустимого размера!'
    };
    this.options = new NgUploaderOptions(opt);
  }

  startUpload(id: number) {
    this.options.data.id = id;
    return this.inputUploadEvents.emit('startUpload');
  }
  calculateSize(): number {
    return ((this.sizeLimit / 1024) / 1024);
  }

  beforeUpload(uploadingFile: UploadedFile): void {
    if (uploadingFile.size > this.sizeLimit) {
      uploadingFile.setAbort();
      this.errorMessage = `Файл привышает допустимый размер. Максимальный размер ${ this.calculateSize() }мб`;
    }
  }

  handleUpload(data: any) {
    this.progress = data.progress;
    setTimeout(() => {
      this.zone.run(() => {
        this.response = data;
        if (data && data.response) {
          this.onResponse.emit(JSON.parse(data.response));
        }
      });
    });
  }

  handlePreviewData(data: any) {
    this.previewData = data;
  }
}
