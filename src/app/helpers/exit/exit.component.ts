import { Component, OnInit } from '@angular/core';
import { Store } from '../../app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-exit',
  templateUrl: './exit.component.html',
  styleUrls: ['./exit.component.css']
})
export class ExitComponent implements OnInit {
  constructor(public store: Store, private router: Router) {}
  ngOnInit() {
    this.store.clearLocalStorage();
    this.store.menuIsVisible = false;
    this.router.navigate(['/']);
  }

}
