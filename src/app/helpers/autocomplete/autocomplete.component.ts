import { Component, OnInit, ElementRef, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'autocomplete',
  host: {
      '(document:click)': 'handleClick($event)',
  },
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.css'],
})
export class AutocompleteComponent {
    @Input() objects: any;
    @Input() label: string;
    @Input() name: string;
    @Output() dataSelected: EventEmitter<string> = new EventEmitter<string>();
    @Input() required: boolean = false;
    @Input() value: string;
    // The internal data model
    public innerValue: any = '';

    public filteredList = [];
    public elementRef;
    constructor(myElement: ElementRef) {
        this.elementRef = myElement;
    }

    ngOnInit() {
        this.innerValue = this.value;
    }

    filter() {
      if (this.innerValue !== '') {
          this.dataSelected.emit(this.innerValue);
          this.filteredList = this.objects.filter(function(el){
              return el.name.toLowerCase().indexOf(this.innerValue.toLowerCase()) > -1;
          }.bind(this));
      }else {
          this.filteredList = [];
      }
    }

    select(item) {
        this.innerValue = item;
        this.filteredList = [];
        this.dataSelected.emit(this.innerValue);
    }

    handleClick(event) {
      let clickedComponent = event.target;
      let inside = false;
      do {
          if (clickedComponent === this.elementRef.nativeElement) {
              inside = true;
          }
          clickedComponent = clickedComponent.parentNode;
      } while (clickedComponent);
        if (!inside) {
            this.filteredList = [];
        }
    }
}
