import {Component } from '@angular/core';
import { Store } from './app.service';
import { RouterModule } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
})
export class NumberChooseComponent {
  private _opened: boolean = false;
  private _modeNum: number = 0;
  private _positionNum: number = 0;
  private _closeOnClickOutside: boolean = false;
  private _showBackdrop: boolean = true;
  private _animate: boolean = true;
  private _trapFocus: boolean = true;
  private _autoFocus: boolean = true;
  private _keyClose: boolean = false;

  private _toggleSidebar() {
    this._opened = !this._opened;
  }
  constructor(public store: Store) {
    try {
      this.store.getBalance();
    }catch (e) {
      alert(e);
    }
  }

}
