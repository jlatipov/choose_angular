import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '../app.service';
import { CheckNumber, CheckNumberAnswer, Auth, AuthAnswer } from './model';

export const PREFIX = '992';
export const STATE_OK = 1;
export const STATE_DOES_NOT_EXIST = 10;
export const STATE_IN_USE = 3;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  // models
  public checkNumber: CheckNumber = new CheckNumber();
  public checkNumberAnswer: CheckNumberAnswer = new CheckNumberAnswer();
  public authModel: Auth = new Auth();
  public authAnswer: AuthAnswer = new AuthAnswer();
  fetching = false;
  public error: string = null;
  // show password files by default hidden 
  public showPassField: boolean = false;


  constructor(
    public store: Store,
    public route: ActivatedRoute,
    private loginService: LoginService,
    private router: Router

  ) {}
  // concat 992 to phone number if number equal to 9 
  getActualMSISDN() {
    console.log(this.checkNumber.msisdn.toString().length);
    return this.checkNumber.msisdn.toString().length === 9 ?
          PREFIX + this.checkNumber.msisdn :
          this.checkNumber.msisdn;
  }

  // submit handler
  submitForm() {
    if (!this.showPassField) {
      this.fetchPin();
    }else {
      this.authRequest(false);
    }
  }

  fetchPin() {
    this.fetching = true;
    this.checkNumber.msisdn = this.getActualMSISDN();
      this.loginService.checkNumber(this.checkNumber).subscribe((data) => {
          this.checkNumberAnswer = data;
          this.authModel.sellerId = this.checkNumberAnswer.id;
          this.validateAnswer();
          this.fetching = false;
      }, (err) => {
          this.fetching = false;
      });
  }

  authRequest(isAutoAuth: Boolean = true) {
    this.fetching = true;
    this.loginService.auth(this.authModel).subscribe((data) => {
          this.authAnswer = data;
          this.validateAuthData(isAutoAuth);
          this.fetching = false;
      }, (err) => {
          this.fetching = false;
          alert('network error');
      });
  }

  validateAuthData(isAutoAuth: Boolean = true) {
    if (this.authAnswer.state === 0) {
      if (!isAutoAuth) {
        this.saveDataToLocalStorage();
      }
      this.store.getBalance();
      this.router.navigate(['/choose']);
    }else {
      this.error = 'Не правилый логин/пароль или просрочен';
    }
  }

  saveDataToLocalStorage() {
    this.checkNumberAnswer['hashedPassword'] = this.authAnswer.hashedPassword;
    window.localStorage.setItem('user', JSON.stringify(this.checkNumberAnswer));
  }

  // TODO need to add state validation
  validateAnswer() {
    switch (this.checkNumberAnswer.state) {
      case STATE_OK:
        this.showPassField = true;
        break;
      case STATE_DOES_NOT_EXIST:
        this.error = 'Такого номера нет в базе данных';
        break;
      default:
        this.error = 'Пожалуйста проверьте номер!';
    }
  }


  ngOnInit() {
    const userData = this.store.getUser();
    if (userData !== undefined && userData !== null) {
      const auth = new Auth();
      auth.pincode = userData.hashedPassword;
      auth.sellerId = userData.id;
      this.authModel = auth;
      this.authRequest(true);
    }
  }
}
