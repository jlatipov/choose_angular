/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
// import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { Observable } from 'rxjs';

import { LoginComponent, STATE_OK, STATE_DOES_NOT_EXIST } from './login.component';
import { Store } from '../app.service';
import { LoginService } from './login.service';
import { CheckNumber, CheckNumberAnswer, Auth, AuthAnswer } from './model';

import {Injectable, ReflectiveInjector} from '@angular/core';
import { fakeAsync, tick} from '@angular/core/testing';
import { BaseRequestOptions, ConnectionBackend, Http, RequestOptions } from '@angular/http';
import { Response, ResponseOptions } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';

const FAKE_CHECK_NUMBER_REQUEST = (checkNumberAnswer: CheckNumberAnswer): any => {
  let result: any;
  this.loginService.checkNumber({aa:"aa"}).subscribe((data: CheckNumberAnswer) => {
    result = data
  }, (err) => {
    result = err
  });

  this.lastConnection.mockRespond(new Response(new ResponseOptions({
    body: {data: checkNumberAnswer},
  })));

  return result;
}


describe('LoginComponent', () => {
  let component: LoginComponent;
  let service: LoginService;

  beforeEach(()=>{
    this.injector = ReflectiveInjector.resolveAndCreate([
      {provide: ConnectionBackend, useClass: MockBackend},
      {provide: RequestOptions, useClass: BaseRequestOptions},
      Http,
      LoginService,
    ]);
    this.loginService = this.injector.get(LoginService);
    this.backend = this.injector.get(ConnectionBackend) as MockBackend;
    this.backend.connections.subscribe((connection: any) => this.lastConnection = connection);


    // this.service = new LoginService(null);
    this.component = new LoginComponent(null, null, this.loginService, null);
    this.component.store = new Store(null, null);
    this.component.router = {
      navigate: (aa)=>{
        return "IT_WORKS";
      }
    }
  });

  it('checkNumber() should query current service url', () => {
    this.loginService.checkNumber({fakeData: "fakeValue"});
    expect(this.lastConnection).toBeDefined('no http service connection at all?');
    expect(this.lastConnection.request.url).toBe("/api/SendPinCode");
  });

  it('checkNumber()', fakeAsync(() => {
       let checkNumberAnswer: CheckNumberAnswer =  new CheckNumberAnswer(12,11,22,33,44,55,66,77);
       let result: any = FAKE_CHECK_NUMBER_REQUEST(checkNumberAnswer);
       tick();
       expect(result.data).toEqual(checkNumberAnswer, 'should contain given amount of heroes');
  }));



  it("should set prefix getActualMSISDN()", () => {
    this.component.checkNumber.msisdn = '928349009';
    let result = this.component.getActualMSISDN();
    expect(result).toBe('992928349009');
  });

  it("should't set prefix getActualMSISDN()", () => {
    this.component.checkNumber.msisdn = '992928349009';
    let result = this.component.getActualMSISDN();
    expect(result).toBe('992928349009');
  });

  it("should validateAnswer() save data to local storage", ()=> {
    const PASSWORD = "3333";
    this.component.authAnswer = new AuthAnswer(0, PASSWORD);
    this.component.validateAuthData(false);
    expect(this.component.store.getUser().hashedPassword).toEqual(PASSWORD);
  });


  it("should showPassField to be true", () => {
    this.component.checkNumberAnswer.state = STATE_OK;
    this.component.validateAnswer();
    expect(this.component.showPassField).toBeTruthy();
  });

  it("should error text to be 'Такого номера нет в базе данных'", () => {
    this.component.checkNumberAnswer.state = STATE_DOES_NOT_EXIST;
    this.component.validateAnswer();
    expect(this.component.showPassField).toBeFalsy();
    expect(this.component.error).toBe('Такого номера нет в базе данных');
  });

  it("should error text to be 'Такого номера нет в базе данных'", () => {
    this.component.checkNumberAnswer.state = 9999;
    this.component.validateAnswer();
    expect(this.component.showPassField).toBeFalsy();
    expect(this.component.error).toBe('Пожалуйста проверьте номер!');
  });
});
