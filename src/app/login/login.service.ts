// Imports
import { Injectable }     from '@angular/core';
import { Http } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { ServiceAbstract } from '../abstract/service';
import {
    AuthAnswer 
    , CheckNumberAnswer } from './model';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class LoginService extends ServiceAbstract {
    private pincodeUrl: string = "/api/SendPinCode";
    private authUrl: string = "/api/SellerAuth";

    constructor (private http: Http) {
        super();
    }

    checkNumber(body): Observable<CheckNumberAnswer> {
        return this.http.post(`${this.pincodeUrl}`, body)
            .map(this.extractData)
            .catch(this.handleError);
    }

    auth(body): Observable<AuthAnswer> {
        return this.http.post(`${this.authUrl}`, body)
            .map(this.extractData)
            .catch(this.handleError);
    }
}