export class CheckNumber {
    public msisdn: string;
    public source: string = "WEB";
    constructor(){}
}

export class CheckNumberAnswer {
    constructor(
        public id: number = 0,
        public regionId: number = 0,
        public sqSquere: number = 0,
        public paymentSystem: number = 0,
        public branchId: number = 0,
        public dealerId: number = 0,
        public userState: number = 0,
        public state: number = 0
    ){}
}


export class Auth {
    public sellerId: number;
    public pincode: string;
}

export class AuthAnswer{
    constructor(
        public state: number = 999,
        public hashedPassword: string = '####'
    ){}
}