export class Spinner {
    spinner: any = null;

    constructor() {
        this.initSpinner();
    }

    initSpinner() {
        if (this.spinner === null) {
            this.spinner = document.getElementsByClassName('spinner')[0];
        }
    }

    showSpinner() {
        if(this.spinner !== null && this.spinner !== undefined)
            this.spinner.style.display = 'block';
    }

    hideSpinner() {
        if(this.spinner !== null && this.spinner !== undefined)
            this.spinner.style.display = 'none';
    }

}