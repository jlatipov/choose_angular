import { Http, Response } from '@angular/http';
import { Spinner } from './spinner';
import {Observable} from 'rxjs/Rx';


export abstract class ServiceAbstract {
    spinner: Spinner = new Spinner();

    protected extractData(res: Response) {
        let body = res.json();
        return body || { };
    }

    protected handleError (error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }


    async fetch(http: Http, url: string, body: any): Promise<any> {
        this.spinner.showSpinner();
        let data: any =  await http.post(url, body)
          .toPromise()
          .then(this.extractData)
          .catch(this.handleError);

        this.spinner.hideSpinner();

        return data;
    }
}
