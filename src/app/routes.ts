import { Routes } from '@angular/router';
import { LoginComponent } from '../app/login';
import { ChooseComponent } from './choose/choose.component';
import { OrderComponent } from './choose/order';
import { RegisterComponent } from './choose/register';
import { ReportComponent } from './choose/report/report.component';
import { SuccessComponent } from './choose/register/success';
import { NoContetentComponentComponent } from './helpers/no-contetent-component/no-contetent-component.component';
import { ExitComponent } from './helpers/exit/exit.component';

export const ROUTES: Routes = [
  { path: '', children: [
    { path: '',     component: LoginComponent },
    { path: 'auth', component: LoginComponent },
  ]},
  {path: 'choose', children: [
    { path: '',              component: ChooseComponent },
    { path: 'order/:id',    component: OrderComponent },
    { path: 'register/:id', component: RegisterComponent },
    { path: 'register/success/:id', component: SuccessComponent },
    { path: 'report',       component: ReportComponent },
  ]},
  { path: 'exit',                component: ExitComponent },
  { path: '**',                  component: NoContetentComponentComponent },
];
