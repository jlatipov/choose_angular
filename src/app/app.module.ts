import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ROUTES } from './routes';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

// none standart libraries
// TODO write my own or wait for materal2 ;)
import { MyDatePickerModule } from 'mydatepicker';
import { NgUploaderModule, NgUploaderService } from 'ngx-uploader';
import { BusyModule, BusyConfig } from 'angular2-busy';
import { SidebarModule } from 'ng-sidebar';

import { RouterModule, PreloadAllModules } from '@angular/router';
import { Store } from './app.service';

import { NumberChooseComponent } from './app.component';
import { LoginComponent, LoginService } from './login';
import { ChooseComponent } from './choose/choose.component';
import { FilterComponent, FilterService } from './choose/filter';
import { OrderComponent, OrderService } from './choose/order';
import { RegisterComponent, RegisterService } from './choose/register';

import { ImageUploaderComponent } from './helpers/image-uploader/image-uploader.component';
import { AutocompleteComponent } from './helpers/autocomplete/autocomplete.component';
import { ReportComponent, ReportService } from './choose/report';
import { NoContetentComponentComponent } from './helpers/no-contetent-component/no-contetent-component.component';
import { ExitComponent } from './helpers/exit/exit.component';
import { SuccessComponent, SuccessService } from './choose/register/success';

import 'hammerjs';
import {
  MdAutocompleteModule,
  MdButtonModule,
  MdButtonToggleModule,
  MdCardModule,
  MdCheckboxModule,
  MdChipsModule,
  MdCoreModule,
  MdDatepickerModule,
  MdDialogModule,
  MdExpansionModule,
  MdGridListModule,
  MdIconModule,
  MdInputModule,
  MdListModule,
  MdMenuModule,
  MdNativeDateModule,
  MdPaginatorModule,
  MdProgressBarModule,
  MdProgressSpinnerModule,
  MdRadioModule,
  MdRippleModule,
  MdSelectModule,
  MdSidenavModule,
  MdSliderModule,
  MdSlideToggleModule,
  MdSnackBarModule,
  MdSortModule,
  MdTableModule,
  MdTabsModule,
  MdToolbarModule,
  MdTooltipModule
} from '@angular/material';
const MATERIAL = [
  MdAutocompleteModule,
  MdButtonModule,
  MdButtonToggleModule,
  MdCardModule,
  MdCheckboxModule,
  MdChipsModule,
  MdCoreModule,
  MdDatepickerModule,
  MdDialogModule,
  MdExpansionModule,
  MdGridListModule,
  MdIconModule,
  MdInputModule,
  MdListModule,
  MdMenuModule,
  MdNativeDateModule,
  MdPaginatorModule,
  MdProgressBarModule,
  MdProgressSpinnerModule,
  MdRadioModule,
  MdRippleModule,
  MdSelectModule,
  MdSidenavModule,
  MdSliderModule,
  MdSlideToggleModule,
  MdSnackBarModule,
  MdSortModule,
  MdTableModule,
  MdTabsModule,
  MdToolbarModule,
  MdTooltipModule
];

@NgModule({
  declarations: [
    NumberChooseComponent,
    LoginComponent,
    ChooseComponent,
    FilterComponent,
    OrderComponent,
    RegisterComponent,
    ImageUploaderComponent,
    AutocompleteComponent,
    ReportComponent,
    NoContetentComponentComponent,
    ExitComponent,
    SuccessComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    NgUploaderModule,
    RouterModule.forRoot(ROUTES, { useHash: true, preloadingStrategy: PreloadAllModules }),
    MyDatePickerModule ,
    SidebarModule,
    MATERIAL
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'ru-RU' },
    NgUploaderService,
    Store,
    LoginService,
    FilterService,
    OrderService,
    RegisterService,
    ReportService,
    SuccessService,
  ],
  bootstrap: [NumberChooseComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
