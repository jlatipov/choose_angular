import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { Store } from '../../app.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FilterService } from './filter.service';
import { Model, Number, NumberList } from './model';
import * as CONSTS from './const';


@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
  @Output() public dataReceived: EventEmitter<NumberList> = new EventEmitter<NumberList>();
  private list: NumberList = new NumberList();
  public model: Model;
  public categories: Object[] = CONSTS.CATEGORIES;
  public prefixes: Object[];
  busy: Subscription;

  constructor(
    public route: ActivatedRoute,
    private filterService: FilterService,
    private router: Router,
    public store: Store
  ) {
    this.initModel();
  }

  ngOnInit() {}
  initModel() {
    const userData = JSON.parse(window.localStorage.getItem(CONSTS.USER_KEY_FROM_LOCAL_STORAGE));
    this.model = new Model(userData.branchId);
    this.prefixes = CONSTS.PREFIXES.filter((i) => {
      return (i.branch ===  this.model.branchId) || i.branch === null;
    });
  }

  loadMore() {
    this.model.pageTo += CONSTS.LOAD_MORE_INCRIMENT;
    this.searchNumber();
  }

  fetchNumbers() {
    this.list = new NumberList();
    this.model.pageTo = 0;
    this.searchNumber();
  }

  prepereToResponse(data) {
    if (this.list.msisdn) {
      this.list.msisdn.push(data.msisdn);
    }
    this.dataReceived.emit(this.list);
  }


  private async searchNumber() {
    let flag = this.store.isChiGap();
    let service = await this.filterService.getList(this.model, flag);
    this.prepereToResponse(service);
  }
}
