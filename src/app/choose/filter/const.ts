export const CATEGORIES = [
    {id:0, name:"Выбор категории"}
    , {id:1, name:"Обычный"}
    , {id:2, name:"Серебрянный"}
    , {id:7, name:"Бронзовый"}
    , {id:3, name:"Золотой"}
    , {id:14, name:"Золотой+"}
]

export const PREFIXES = [
    {id:"99292", name:"92", branch:0}
    , {id:"99293", name:"93", branch:2}    
    , {id:"99277", name:"77", branch: null}
    , {id:"99250", name:"50", branch: null} 
]

export const USER_KEY_FROM_LOCAL_STORAGE: string = "user";
export const LOAD_MORE_INCRIMENT: number = 10; 

