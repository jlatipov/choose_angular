
export class Model {
    public pattern: string = "";
    public category: string = '0';
    public prefix: string = '99277';
    public pageTo:number =  0;
    constructor(public branchId:number){}
}

export class NumberList{
    public msisdn: Number[] = []; 
}

export class Number {
    public id: number;
    public branchId: number;
    public msisdn: string;
    public phoneClass: string;
    public price: number; 
} 