// Imports
import { Injectable }     from '@angular/core';
import { Http } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { ServiceAbstract } from '../../abstract/service';
import { NumberList } from './model';

// Import RxJs required methods

@Injectable()
export class FilterService extends ServiceAbstract {
    private getNumbersUrl: string = "/api/GetNumbers";
    private getNumbersChiGapUrl: string = "/api/GetNumbersChiGap";

    constructor (private http: Http) {
        super();
    }

    getList(body: any, chiGapFlag: boolean = false): Promise<NumberList> {
      let URL = "";
      if(chiGapFlag !== true ){ URL = this.getNumbersUrl;}
      else {URL = this.getNumbersChiGapUrl;}
      return this.fetch(this.http, URL, body);
    }
}
