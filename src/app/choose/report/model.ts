export class  Model {
    id: number;
    regDate: string;
    fullName: string;
    number: string;
    msg: string;
    icc: string;
}
