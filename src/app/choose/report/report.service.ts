import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ServiceAbstract } from '../../abstract/service';
import { Model } from './model';

@Injectable()
export class ReportService extends ServiceAbstract {
    private getReportUrl: string = '/api/Report';

    constructor (private http: Http) {
        super();
    }

    getReport(body): Promise<Model[]> {
        return this.fetch(this.http, this.getReportUrl, body);
    }
}
