import { Component, OnInit } from '@angular/core';
import { Location }                              from '@angular/common';
import { ActivatedRoute, Router }                from '@angular/router';
import { Store }                                 from '../../app.service';
import { OrderService }                          from '../order/order.service';
import { ReportService } from './report.service';
import { Model } from './model';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  public reports: any = [new Model()];
  constructor(public store: Store,
    public route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private reportService: ReportService,
    private orderService: OrderService) {
    this.setData();
    //   .subscribe((data) => {
    //     this.reports = data;
    //   }, (err) => {
    //     console.log(err);
    //   });
  }

  async setData() {
        this.reports = await this.reportService.getReport({id: this.store.getUser().id});
  }

  setLabel(status) {
        switch (status) {
            case 0:
                return {label: 'error', msg: 'Заказ ожидает обработку'};
            case 1:
                return {label: 'warning', msg: 'Заказ принят на обработку'};
            case 2:
                return {label: 'error', msg: 'Ошибка Заказа'};
            case 9:
                return {label: 'success', msg: 'Заказа прошел успешно'};
            case 999:
                return {label: 'error', msg: 'Ошибка заказа'};
            default:
                return {label: 'error', msg: 'Неизвестная ошибка, просим сообщить нам'};
        }
    }

  ngOnInit() {
  }

}
