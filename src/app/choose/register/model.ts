export class RequestModel {
    sellerId: number = 0;
    strPhoneNumber: string = '';
    regType: number = 0;
    strLastName: string = '';
    strName: string = '';
    strSecondName: string = '';
    txtDateBirth: any = new Date(1990, 0, 1);
    strIcc: string = '';
    gender: number = 0;
    txtCountry: string = '';
    txtRegion: string = '';
    txtDistrict: string = '';
    txtCity: string = '';
    txtJamoat: string = '';
    txtVillage: string = '';
    txtStreet: string = '';
    txtHouse: string = '';
    email: string = '';
    public passType: boolean = false;
    txtPassportSerias: string = 'А';
    txtPassportNumber: string = '';
    txtPassportPlaceType: string = 'ОВД';
    txtPassportPlace: string = '';
    txtPassportWhen: any;
    imageFront: boolean = false;
    imageBack: boolean = false;
    orderId: number = 0;
}

export class ResponseModel {
    id: number;
    state: string;
}
