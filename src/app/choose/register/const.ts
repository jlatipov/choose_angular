export const PASS_SERIES = [
     { name: "А", id: "А", innerType: true }
    , { name: "P", id: "P", innerType: false }
    , { name: "M", id: "M", innerType: false }
    , { name: "U", id: "U", innerType: false }
]
export const PASS_PLACES = [
    { name: 'ОВД', id: 'ОВД'}
    , { name: 'МИД', id: 'МИД', innerType: false }
];


export const RESPONSE_OK = 'OK';
