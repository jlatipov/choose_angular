// Imports
import { Injectable }     from '@angular/core';
import { Http } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { ServiceAbstract } from '../../abstract/service';
import { ResponseModel } from './model';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class RegisterService extends ServiceAbstract {
    private getLocationsUrl: string = '/api/GetLocations';
    private getUserDataUrl: string = '/api/FillForm';
    private getBpRegisterUrl: string = '/api/BpRegister';

    constructor (private http: Http) {
        super();
    }

    getLocations(body): Promise<ResponseModel> {
      return this.fetch(this.http, this.getLocationsUrl, body);
    }

    insertUserData(body): Promise<ResponseModel> {
        let newBody = Object.assign({},  body);
        newBody.txtDateBirth = newBody.txtDateBirth.formatted;
        newBody.txtPassportWhen = newBody.txtPassportWhen.formatted;
        return this.fetch(this.http, this.getUserDataUrl, newBody);
    }

    bpRegister(body): Promise<ResponseModel> {
        return this.fetch(this.http, this.getBpRegisterUrl, body);
    }
}
