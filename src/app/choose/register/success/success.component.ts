import { Component, OnInit }      from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SuccessService }         from './success.service';
import { Model }                  from './model';
import { Store }                  from '../../../app.service';


@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.css']
})
export class SuccessComponent implements OnInit {
  model: Model = new Model();
  id: number;

  constructor(public store: Store,
    public route: ActivatedRoute,
    private successService: SuccessService,
    private router: Router) { }

  ngOnInit() {
    this.id = this.route.params['value'].id;
    this.fetchStatus();
    this.store.getBalance();
  }

  refresh() {
    this.fetchStatus();
  }

  fetchStatus() {
    this.successService.fetchNumberStatus({id: this.id}).subscribe(
      data => this.model = data,
      err => console.log(err)
    );
  }

  home() {
    this.router.navigate([`/choose`]);
  }

  getStatus(status: number): any {
    switch (status) {
      case 1:
        return {text: 'Обрабатывается', label: 'warning'};

      case 9:
        return {text: 'Успешно подключен', label: 'success'};

      case 999:
        return {text: 'Не правильно введены данные просим попробовать еще раз', label: 'error'};
      case 2:
        return {text: 'Ошибка просим обратится в администрацию', label: 'error'};
      default:
        return {text: 'Принят на обработку', label: 'error'};
    }
  }
}
