// Imports
import { Injectable }     from '@angular/core';
import { Http } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { ServiceAbstract } from '../../../abstract/service';
import { Model } from './model';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class SuccessService extends ServiceAbstract {
    private checkNumberStatusUrl: string = 'api/CheckOrderStatus';

    constructor (private http: Http) {
        super();
    }

    fetchNumberStatus(body): Observable<Model> {
        return this.http.post(this.checkNumberStatusUrl, body)
            .map(this.extractData)
            .catch(this.handleError);
    }
}
