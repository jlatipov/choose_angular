import { Component, OnInit, ViewChild }          from '@angular/core';
import { Subscription }                          from 'rxjs';
import { Location }                              from '@angular/common';
import { ActivatedRoute, Router }                from '@angular/router';
import { RegisterService }                       from './register.service';
import { Store, LocationItems }                  from '../../app.service';
import { RequestModel }                          from './model';
import { OrderService }                          from '../order/order.service';
import { IMyOptions }                            from 'mydatepicker';
import { PASS_PLACES, PASS_SERIES, RESPONSE_OK } from './const';
import { ImageUploaderComponent }                from '../../helpers/image-uploader/image-uploader.component';
import { ResponseModel }                         from './model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  @ViewChild('firstImage') firstImage: ImageUploaderComponent;
  @ViewChild('secondImage') secondImage: ImageUploaderComponent;

  public model: RequestModel = new RequestModel();
  public PASS_PLACES = PASS_PLACES;
  public PASS_SERIES = PASS_SERIES;
  public message: string = 'Поля, отмеченные звездочкой, обязательны для заполнения.';
  public MESSAGE_FIELD_REQIRED = 'Обязательное поле';
  public responseModel: ResponseModel = new ResponseModel();
  public country: LocationItems = new LocationItems();
  public region: LocationItems = new LocationItems();
  public district: LocationItems = new LocationItems();
  public city: LocationItems = new LocationItems();

  firstImageFlag: boolean = false;
  secondImageFlag: boolean = false;
  busy: Subscription;

  constructor(public store: Store,
    public route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private registerService: RegisterService ,
    private orderService: OrderService) {}

  openSnackBar(message: string, action: string) {
    let element: any = document.querySelector('#errorMessages');
    element.style.display = 'block';
    element.querySelector('.message').innerHTML = message;
  }

  isManSetted() {
    return this.model.gender === 0 ? true : false;
  }

  fetchLocations() {
    const LOCATIONS = ['country', 'region', 'district', 'city'];
    LOCATIONS.map((loc) => {
      this[loc] = this.store.isItInLocalStorage(loc);
      if (!this.store.isItInLocalStorage(loc)) {
        this.store.fetchLocation(loc, '').subscribe((data: LocationItems) => {
          const LOCATION = JSON.stringify(data.locationItems);
          window.localStorage.setItem(loc, LOCATION);
          this[loc] = data.locationItems;
        }, (err) => {

        });
      }
    });
  }

  getPassportPlaces() {
    return this.filterArray(PASS_PLACES);
  }

  getPassportSeries() {
    return this.filterArray(PASS_SERIES);
  }

  filterArray(object: any) {
    return object.filter((i: any) => {
      return i.innerType == undefined || (this.model.passType != i.innerType);
    });
  }

  loadNumber() {
    let choosedNumber = this.store.getChoosedNumber();
    this.model.sellerId = this.store.getUser().id;
    if (choosedNumber) {
      this.model.strPhoneNumber = choosedNumber.msisdn;
    }else {
      this.getModelFromNetwork();
    }
  }

  getModelFromNetwork() {
    const id = this.route.params['value'].id;
    this.busy = this.orderService.getNumberPrice({id: id}).subscribe((data) => {
      this.model.strPhoneNumber =  data.msisdn;
    }, (err) => {
      console.log(err);
    });
  }

  ngOnInit() {
    const filledForm = this.store.getFilledFormFromLocalStorage();
    if (filledForm) {
      this.model = filledForm;
    }
    this.loadNumber();
    this.fetchLocations();
  }

  getPassTypeLen() {
    const INNER_LEN = 7;
    const OUTER_LEN = 9;
    return this.model.passType ? OUTER_LEN : INNER_LEN;
  }

  // TODO ask from Mubin ako what i need to do save form with id or not
  saveDataOnLocalStorage() {
    // window.localStorage.setItem('filledForm', JSON.stringify(Object.assign({}, this.model, { orderId: 0 })));
    window.localStorage.setItem('filledForm', JSON.stringify(this.model));
  }

  register(form: any, event: Event) {
    event.preventDefault();
    this.saveDataOnLocalStorage();
    this.prepareDataBeforeSend();


    if (form.valid) {
      if (this.firstImage.fileIsSelected()) {
        if (this.model.passType || this.secondImage.fileIsSelected()) {
          this.insertUserData();
        }else {this.openSnackBar('Фото №2 не выбрано!', 'OK'); }
      }else {this.openSnackBar('Фото №1 не выбрано!', 'OK'); }
    }else {this.openSnackBar('Пожалуйста заполните поля с звоздочкой', 'OK'); }
  }

  prepareDataBeforeSend() {
    this.model.regType = this.store.isChiGap() ? 2 : 1;
  }


  async insertUserData() {
    let data: Promise<ResponseModel> = this.registerService.insertUserData(this.model);
    data.then((d) => {
      this.responseModel = d;

      if (d.state === RESPONSE_OK) {
        this.bpRegister();
      }else {
        this.openSnackBar(d.state, 'OK');
      }
      this.model.orderId = this.responseModel.id;
    });
  }

  async bpRegister() {
    let data = await this.registerService.bpRegister(this.responseModel);
    // if (this.model.orderId === 0) {
      if (data.state === RESPONSE_OK) {
        this.firstImage.startUpload(this.model.orderId);
      }else {
        this.openSnackBar(data.state, 'OK');
      }
    // }
  }


  imageOneProgress(event) {
    this.toggleDisableForm(false);
    if (event.state === RESPONSE_OK && !this.firstImageFlag) {
      if(!this.model.passType){
        this.secondImage.startUpload(this.responseModel.id)
      }else{
        this.secondImageFlag = true;
        this.router.navigate([`/choose/register/success`, this.responseModel.id]);
        this.clear();
      }
      this.firstImageFlag = true;
    }else {}
  }

  imageTwoProgress(event) {
    this.toggleDisableForm(false);
    if (event.state === RESPONSE_OK && !this.secondImageFlag) {
      this.router.navigate([`/choose/register/success`, this.responseModel.id]);
      this.clear();
    }else {}
    this.secondImageFlag = true;
  }

  back() {
    this.location.back();
  }

  clear() {
    this.model = new RequestModel();
    this.store.removeFilledFormFromLocalStorage();
  }

  printImageUploadEvents(event: any) {
    this.toggleDisableForm(true);
  }

  toggleDisableForm(toggle: boolean) {
    let formItems: any = document.querySelectorAll('.register--inputs');
    let buttons = formItems[1].querySelectorAll('button');
    let OPACITY = toggle ? '0.2' : '1';
    let DISABLE = toggle ? 'disabled' : '';

    formItems[0].style.opacity = OPACITY;
    formItems[1].style.opacity = OPACITY;

    buttons[0].disabled = DISABLE;
    buttons[1].disabled = DISABLE;
    buttons[2].disabled = DISABLE;

  }

  checkPassType(obj) {
    return obj.innerType == undefined || (this.model.passType != obj.innerType);
  }

  capitalizeString(str) {
      return str.replace(/\w\S*/g, function(txt){ return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
  }

  toUppercaseLastName($event) {
    this.model.strLastName = this.capitalizeString($event);
  }

  toUppercaseFirstName($event) {
    this.model.strName = this.capitalizeString($event);
  }

  toUppercaseSecondName($event) {
    this.model.strSecondName = this.capitalizeString($event);
  }
}
