import { Component, OnInit, ViewChild } from '@angular/core';
import { Store } from '../app.service';
import { NumberList } from './filter/model';
import { ActivatedRoute, Router } from '@angular/router';
import { FilterComponent } from './filter'

@Component({
  selector: 'app-choose',
  templateUrl: './choose.component.html',
  styleUrls: ['./choose.component.css']
})
export class ChooseComponent implements OnInit {
  @ViewChild(FilterComponent) filter: FilterComponent;
  public numberList = null;
  constructor(
    public store: Store,
    public route: ActivatedRoute,
    private router: Router) {}

  ngOnInit() {
    this.numberList  = this.store.getNumbers() ? this.store.getNumbers() : null ;
  }

  goToConfirmation(id: number) {
      this.router.navigate([`/choose/order`, id]);
  }

  dataWasRecieved(numbers: NumberList) {
    this.store.setNumbers(numbers);
    this.numberList = numbers;
  }

  loadMore(){
    this.filter.loadMore();
  }
  getSimClass(phoneClass: string) {
    switch (phoneClass) {
      case 'Обычный':
        return 'simple';
      case 'Серебрянный':
        return 'silver';
      case 'Бронзовый':
        return 'bronze';
      case 'Золотой':
        return 'gold';
      default:
          return 'simple';
    }
  }
  showNumbers() {
    return this.numberList != null && this.numberList.msisdn[0].length > 0;
  }
}
