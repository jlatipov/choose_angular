export class RequestModel {
    id: number;
}

export class ResponseModel {
    id: number;
    branchId: number;
    msisdn: string;
    phoneClass: string;
    price: number;
}
