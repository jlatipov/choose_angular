// Imports
import { Injectable }     from '@angular/core';
import { Http } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { ServiceAbstract } from '../../abstract/service';
import { ResponseModel } from './model';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class OrderService extends ServiceAbstract {
    
    private getNumberPriceUrl: string = "/api/GetNumerPrice";

    constructor (private http: Http) {
        super();
    }
   

    getNumberPrice(body): Observable<ResponseModel> {
        return this.http.post(this.getNumberPriceUrl, body)
            .map(this.extractData)
            .catch(this.handleError);
    }
}