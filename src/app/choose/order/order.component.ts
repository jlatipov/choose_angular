import { Component, OnInit }      from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderService }           from './order.service';
import { ResponseModel }          from './model';
import { RequestModel } from '../register/model';
import { Location }               from '@angular/common';
import { Store } from '../../app.service';


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  model: ResponseModel = new ResponseModel();
  id: number;
  busy: Subscription;
  filledForm: RequestModel;

  constructor(
    public store: Store,
    public route: ActivatedRoute,
    private orderService: OrderService,
    private router: Router,
    private location: Location
    ) {}

  ngOnInit() {
    this.id = this.route.params['value'].id;
    this.getNumberPrice(this.id);
    this.store.getBalance();
    this.filledForm = this.store.getFilledFormFromLocalStorage();
  }

  goToRegister() {
    this.store.setChoosedNumber(this.model);
    this.redirectToRegisterPage();
  }
  redirectToRegisterPage() {
    this.router.navigate([`/choose/register`, this.id]);
  }

  goBack() {
    this.location.back();
  }

  removeOrder() {
    this.store.removeFilledFormFromLocalStorage();
    this.goBack();
  }

  getNumberPrice(id: number) {
    this.busy = this.orderService.getNumberPrice({id: id}).subscribe((data) => {
      this.model =  data;
    }, (err) => {
      console.log(err);
    });
  }
}
