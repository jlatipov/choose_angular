/* tslint:disable:no-unused-variable */

import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { NumberChooseComponent } from './app.component';
import { Store, LocationItem, BalanceModel, LocationItems } from './app.service';
import { BaseRequestOptions, ConnectionBackend, Http, RequestOptions, Response, ResponseOptions} from '@angular/http';
import { Router } from '@angular/router';
import { ReflectiveInjector } from '@angular/core';

describe('App: Tcell', () => {
  beforeEach(() => {
    this.injector = ReflectiveInjector.resolveAndCreate([
       {provide: ConnectionBackend, useClass: MockBackend},
       {provide: RequestOptions, useClass: BaseRequestOptions},
       { provide: Router, useClass: null },
       Http,
       Store
     ]);
     this.store = this.injector.get(Store);
     this.backend = this.injector.get(ConnectionBackend) as MockBackend;
     this.backend.connections.subscribe((connection: any) => {
        this.lastConnection = connection;
      });
  });

  it('should check balance url', () => {
      expect(this.store.balanceUrl).toBe('/api/GetBalance');
  });

  it('should check location url', () => {
    expect(this.store.getLocationUrl).toBe('api/GetLocations');
  });

  it('should exist data at LocalStorage', () => {
    const STORAGE_KEY = 'testLocation';
    const EXPECTED_NAME = 'Khujand';
    window.localStorage.setItem(STORAGE_KEY, JSON.stringify([new LocationItem(1, EXPECTED_NAME)]));
    let dd = this.store.isItInLocalStorage(STORAGE_KEY);
    expect(dd[0].name).toBe(EXPECTED_NAME);
  });

  it('should clear LocalStorage', () => {
    const STORAGE_KEY = 'testLocation';
    window.localStorage.setItem(STORAGE_KEY, JSON.stringify([new LocationItem(1)]));
    this.store.clearLocalStorage();
    let dd = this.store.isItInLocalStorage(STORAGE_KEY);
    expect(dd).toBe(null);
  });

  it('should fetch balance', fakeAsync(() => {
    // TODO fix this test
    // let result: BalanceModel[];
    // const RESPONSE_MODEL = [new BalanceModel(1,"hello", 10), new BalanceModel(2,"world", 20)];
    // this.store.getBalance('URL', {id:2}).then((balance: BalanceModel[]) => result = balance);
    // this.lastConnection.mockRespond(new Response(new ResponseOptions({
    //   body: JSON.stringify({data: RESPONSE_MODEL}),
    // })));
    // tick();
    // expect(result.length).toEqual(2, 'should contain given amount of balance');
    // expect(result[0]).toEqual(RESPONSE_MODEL[0], ' should be the first');
    // expect(result[1]).toEqual(RESPONSE_MODEL[1], ' should be the second');
  }));
});
