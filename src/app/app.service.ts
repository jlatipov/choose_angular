import { Injectable }     from '@angular/core';
import { Http } from '@angular/http';
import { NumberList } from './choose/filter/model';
import { ServiceAbstract } from './abstract/service';
import { ResponseModel } from './choose/order/model';
import { RequestModel as RegisterModel } from './choose/register/model';
import { Router } from '@angular/router';
import {Observable} from 'rxjs/Rx';


class UserModel {
    id: number;
    regionId: number;
    spSquere: number;
    branchId: number;
    dealerId: number;
    paymentSystem: number;
    userState: number;
    state: number;

    sellerPointId: string;
    adress: string;
    sellerName: string;
    sellerContactPhone: string;
    spCategory: string;
    spOwnerPhone: string;
    registrationDate: string;
    msisdn: string;
    hashedPassword: string;
}

export class BalanceModel {
    constructor(
        public state: number = 999,
        public msg: string = '',
        public amount: number = 0
    ){}
    
}

export class LocationItems {
    locationItems: LocationItem[];
}
export class LocationItem {
    constructor(
        public id: number = 0,
        public name: string = ''
    ){}
}


@Injectable()
export class Store extends ServiceAbstract {
    public numberList: NumberList;
    public choosedNumber: ResponseModel;
    public balanceUrl: string = '/api/GetBalance';
    public getLocationUrl: string = 'api/GetLocations';
    public menuIsVisible: boolean = false;
    public balance: BalanceModel = new BalanceModel();
    constructor(public http: Http, public router: Router) {
        super();
    }

    isItInLocalStorage(object: string): LocationItem[] {
        return JSON.parse(window.localStorage.getItem(object));
    }

    async getBalance() {
        let user = this.getUser();
        if (user) {
            let data = await this.fetch(this.http, `${this.balanceUrl}`, {id: user.id, pincode: user.hashedPassword});
            this.validateBalance(data);
            return data;
        }
    }

    validateBalance(data: BalanceModel) {
        if (data.state === 0) {
            this.balance = data;
            this.menuIsVisible = true;
        }else {
           this.router.navigate(['exit']);
        }
    }

    fetchLocation(object: string, pattern: string): Observable<LocationItems> {
        return this.http.post(`${this.getLocationUrl}`, {object: object, pattern: pattern})
            .map(this.extractData)
            .catch(this.handleError);
    }

    getUser(): UserModel {
        return JSON.parse(window.localStorage.getItem('user'));
    }

    getNumbers(): NumberList {
        return this.numberList;
    }
    setNumbers(numberList: NumberList) {
        this.numberList = numberList;
    }

    setChoosedNumber(choosed: ResponseModel) {
        this.choosedNumber = choosed;
    }

    getChoosedNumber(): ResponseModel {
        return this.choosedNumber;
    }

    getFilledFormFromLocalStorage(): RegisterModel {
        return JSON.parse(window.localStorage.getItem('filledForm'));
    }

    removeFilledFormFromLocalStorage() {
        window.localStorage.removeItem('filledForm');
    }

    clearLocalStorage() {
        window.localStorage.clear();
    }

    isChiGap() {
        const CHI_GAP_REGION = 1117;
        return CHI_GAP_REGION === this.getUser().regionId ? true : false;
    }
}
