import { PaymonPage } from './app.po';

describe('paymon App', function() {
  let page: PaymonPage;

  beforeEach(() => {
    page = new PaymonPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
